#!/bin/sh
$(aws ecr get-login --no-include-email --region us-east-1)
docker run -it -e DB_HOST={DB_HOST} -e DB_NAME={DB_NAME} -e DB_USER={DB_USER} -e DB_PASS={DB_PASS} sh -c "python manage.py wait_for_db && python manage.py createsuperuser"